package com.endava.demo;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;
@Data
@Builder
public class Event {
    private String title;
    private String type;
    private String description;
    private LocalDate date;
    private String privacy;


}
