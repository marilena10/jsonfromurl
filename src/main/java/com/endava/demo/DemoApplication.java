package com.endava.demo;

import com.google.gson.*;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jms.annotation.EnableJms;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Scanner;

import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootApplication
//@EnableJms
public class DemoApplication {

	public static void main(String[] args) throws IOException {
		SpringApplication.run(DemoApplication.class, args);
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		String sURL = "https://wishlist-app-backend.herokuapp.com/wishlists/wishlist/317";
		String URL = "https://wishlist-app-backend.herokuapp.com/wishlists";
		JsonArray jsonArray=JsonReader.readJsonArrayFromUrl(URL);
		JsonObject jsonObject=JsonReader.readJsonObjectFromUrl(sURL);
		System.out.println(jsonObject.toString());
		String[] arr = jsonArray.toString().replace("},{", " ,").split(" ");
		for(int i=0;i< arr.length;i++){
		System.out.println(arr[i]);
		i++;
		}
//		// Connect to the URL using java's native library
//		URL url = new URL(sURL);
//		URLConnection request = url.openConnection();
//		request.connect();
//
//		// Convert to a JSON object to print data
//		JsonParser jp = new JsonParser(); //from gson
//		JsonElement root = jp.parse(new InputStreamReader((InputStream) request.getContent())); //Convert the input stream to a json element
//		JsonArray rootobj = root.getAsJsonArray();
		//JSONArray json = JsonReader.readJsonFromUrl("https://wishlist-app-backend.herokuapp.com/wishlists");
		//System.out.println(json.toString());
//	String string=json.toString().replace("{","[").replace("}","]");
//	ObjectMapper mapper = new ObjectMapper();
//	List<Event> listEvent = mapper.readValue(string, List.class);
//	listEvent.stream().forEach(m-> System.out.println(m));
//  	Gson gson = new Gson();
//  	for(int i=0;i<json.length();i++)
//	{
//		JSONObject jsonObject=json.getJSONObject(i);
//		Event event= Event.builder()
//			.date(LocalDate.parse((CharSequence) jsonObject.get("date"), formatter))
//			.description((String)jsonObject.get("description"))
//			.title((String)jsonObject.get("title"))
//			.build();
//		System.out.println(event);
//	}

	}
}
